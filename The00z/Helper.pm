package The00z::Helper;

sub
new
{
  my ($pkg, $options) = @_;

  my $self = $options;

  bless($self, $pkg);

  return $self;
}

sub
show_header 
{
  my $self = shift;

  return unless $self->{'<header>'};

  $self->{'<header>'} =~ s/[[:space:]]+/ /g;

  my $format_top="format STDERR_TOP =\n"
    . "^" . '<' x 78 . "~\n"
    . '$self->{\'<header>\'}' . "\n"
    . '~~^'. '<'x 76 . "\n"
    . '$self->{\'<header>\'}' . "\n"
    . ".\n";

  eval "$format_top";
}

# Shows the help of an option (prints the formatted value of the hash %options
# using the `option' has a key)
sub
show_options
{
  my $self = shift;

  my $format = "format STDERR =\n"
    . "~~^". '<' x 15 . " ^" . '<' x 65 . "\n"
    . '$name, $description'. "\n"
    . ".\n";

  eval "$format";

  while ( ($name, $description ) = each %$self)
  {
    $description =~ s/[[:space:]]+/ /g;
    write STDERR if $name ne '<header>';
  }
}

sub
show
{
  my ($self) = @_;

  $self->show_header;

  $self->show_options;
}

sub
DESTROY
{
  my $self = @_;

  %$self = ();
  
}

1;
