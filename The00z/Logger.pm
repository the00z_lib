package The00z::Logger;

use IO::File;
use Carp qw(croak);

our $VERSION = '0.0.1';

sub 
new
{
  my ($pkg,$logfile) =  @_;

  my $self = {};

  my $filehandler = IO::File->new(">>$logfile") 
    or croak "Cannot open $logfile\n";

  $self->{'filehandler'} = $filehandler;
  $self->{'level'} = 1;
  $self->{'logfile'} = $logfile;

  $self->{'filehandler'}->print("Opening "
      . $self->{'logfile'} . " : " . localtime(time) .  "\n");

  bless($self,$pkg);

  return $self;
}

sub
write
{
  my ($self, $level) = (shift, shift); 

  if ($level <= $self->{'level'})
  {
    $self->{'filehandler'}->print($_) for @_;
  }
}

sub
level
{
  my ($self, $level) = @_;
  $self->{'level'} = $level 
    if @_ == 2 and $level =~ m/^\d+$/;
  return $self->{'level'};
}

sub
DESTROY
{
  my $self = shift;

  $self->write($self->{'level'} ,"Closing ". $self->{'logfile'} . " : " 
      . localtime(time) . "\n");

  $self->{'filehandler'}->close() 
    or croak "Unable to write " . $self->{'logfile'} . "\n"; 

  %{$self} = ();
}

1;
